% Viajando por GIT
% Centro de computación - UNET

% Raúl Andrés Casanova Sosa
% raul.casanova@unet.edu.ve

# Viajando por GIT

+ ¿Qué es GIT?
+ Beneficios
+ Historia
+ Propuesta para la UNET

## Clientes de GIT

+ CLI (Command Line Interface)
+ GUIs
	+ [SourceTree](https://www.sourcetreeapp.com/)
	+ [GitKraken](https://www.gitkraken.com/) 
	+ [Otros](https://git-scm.com/downloads/guis) 

## Learn GIT, not commands

+ Configuracion 
	`git config --global user.name "Coco Casanova"`
	`git config --global user.email "elcoco@unet.edu.ve"`
+ Credenciales en Git[Lab/Hub]
